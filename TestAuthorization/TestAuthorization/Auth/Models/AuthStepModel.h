//
//  AuthStepModel.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthStepModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthStepModel : NSObject <AuthStepModelProtocol>

@end

NS_ASSUME_NONNULL_END
