//
//  AuthStepModel.m
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "AuthStepModel.h"

@interface AuthStepModel ()

@property (strong, nonatomic) NSString *currentPinCode;
@property (assign, nonatomic) AuthStep currentStep;

@end

@implementation AuthStepModel

- (instancetype)init {
    self = [super init];
    if (nil != self) {
        [self configure];
    }
    return self;
}

- (void)configure {
    self.currentPinCode = nil;
}

- (NSString *)stateTitle {
    NSString *title = @"Введите код, чтобы\nразблокировать приложение";
    switch (self.currentStep) {
        case AuthStepStart:
            title = @"Придумайте четырехзначный пин-код";
            break;
        case AuthStepСonfirmation:
            title = @"Повторите придуманный пин-код";
            break;
        case AuthStepError:
            title = @"Введенные коды не совпадают, пожалуйста, попробуйте снова";
            break;
        case AuthStepSuccess:
            title = @"Успешно";
            break;
        default:
            break;
    }
    return title;
}

- (void)resetPinCode {
    self.currentPinCode = nil;
}

- (void)setPinCode:(NSString *)pinCode {
    switch (self.currentStep) {
        case AuthStepStart:
        case AuthStepError:
        case AuthStepSuccess:
            self.currentPinCode = pinCode;
            self.currentStep = AuthStepСonfirmation;
            break;
        case AuthStepСonfirmation:
            if ([(pinCode ? : @"") isEqualToString:(self.currentPinCode ? : @"")]) {
                self.currentStep = AuthStepSuccess;
            } else {
                self.currentStep = AuthStepError;
            }
            break;
        default:
            break;
    }
}

- (void)setCurrentPinCode:(NSString *)currentPinCode {
    _currentPinCode = currentPinCode;
    if (nil == _currentPinCode) {
        self.currentStep = AuthStepStart;
    } else {
        self.currentStep = AuthStepСonfirmation;
    }
}

@end
