//
//  AuthModel.m
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "AuthModel.h"
#import "AuthStepModel.h"

@implementation AuthModel

+ (id <AuthStepModelProtocol>)authStepModel {
    return [[AuthStepModel alloc] init];
}

@end
