//
//  AuthModel.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthStepModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

#define kPinLenght 4

@interface AuthModel : NSObject

+ (id <AuthStepModelProtocol>)authStepModel;

@end

NS_ASSUME_NONNULL_END
