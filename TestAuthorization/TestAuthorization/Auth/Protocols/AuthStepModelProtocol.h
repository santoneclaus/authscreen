//
//  AuthStepModelProtocol.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum {
    AuthStepStart           = 0,//pin entry first time
    AuthStepСonfirmation    = 1,//pin entry confirmation
    AuthStepError           = 2,//re-entry error
    AuthStepSuccess         = 3//finish
} AuthStep;

@protocol AuthStepModelProtocol <NSObject>
/**
 title for current state
 */
- (NSString *)stateTitle;

- (void)resetPinCode;
- (void)setPinCode:(NSString *)pinCode;
@property (assign, nonatomic, readonly) AuthStep currentStep;

@end

NS_ASSUME_NONNULL_END
