//
//  AuthViewProtocol.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AuthViewProtocol <NSObject>

- (void)configureWithTitle:(NSString *)title;
/**
 entered pin
 */
@property (strong, nonatomic, readonly) NSString *pinCod;
/**
 input pincode length
 */
@property (assign, nonatomic) NSUInteger pinCodLenght;
/**
 remove all  symbols
 */
- (void)clearPinCcod;

@end

NS_ASSUME_NONNULL_END
