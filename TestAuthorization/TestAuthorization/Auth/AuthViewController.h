//
//  AuthViewController.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AuthViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
