//
//  AuthViewController.m
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "AuthViewController.h"
#import "AuthViewProtocol.h"
#import "AuthModel.h"

@interface AuthViewController ()

@property (weak, nonatomic) IBOutlet UIView <AuthViewProtocol> *authView;
@property (strong, nonatomic) id<AuthStepModelProtocol>authModel;

@end

@implementation AuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configure];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)configure {
    self.authModel = [AuthModel authStepModel];
    [self updateTitle];
    self.authView.pinCodLenght = kPinLenght;
}

- (void)updateTitle {
    [self.authView configureWithTitle:[self.authModel stateTitle]];
    [self finishEnterPincod];
}

#pragma mark - Actions
- (IBAction)tapDoneButton:(UIButton *)button {
    [self.authModel setPinCode:self.authView.pinCod];
    [self.authView clearPinCcod];
    [self updateTitle];
}

- (void)finishEnterPincod {
    if (AuthStepSuccess == self.authModel.currentStep) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:@"Коды совпали"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        //Add Buttons
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ок"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {

                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//#pragma mark - Interface rotation
//- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator  {
//    [self.authView setHorizontalOrientation:size.height > size.width];
//}

@end
