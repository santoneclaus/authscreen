//
//  GradientView.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface GradientView : UIView

@property (strong, nonatomic) IBInspectable UIColor *startColor;
@property (strong, nonatomic) IBInspectable UIColor *middleColor;
@property (strong, nonatomic) IBInspectable UIColor *endColor;

@end

NS_ASSUME_NONNULL_END
