//
//  GradientView.m
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "GradientView.h"

@interface GradientView () {
    CGRect _previousLayerRect;
}

@property (nonatomic, weak) CAGradientLayer *gradientLayer;

@end

@implementation GradientView

- (void)checkColors {
    if (nil == self.startColor) {
        self.startColor = [UIColor clearColor];
    } else {
        self.backgroundColor = self.startColor;
    }
    if (nil == self.endColor) {
        self.endColor = [UIColor clearColor];
    }
    if (nil == self.endColor) {
        self.middleColor = [UIColor clearColor];
    }
}
//TODO: повесил градиент от угла, проще взять фон с ресурсов
- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (!CGRectEqualToRect(self.bounds, _previousLayerRect)) {
        _previousLayerRect = self.bounds;
        [self checkColors];
        if (nil == self.gradientLayer) {
            CAGradientLayer *layer = [CAGradientLayer layer];
//            layer.colors = @[(__bridge id)self.startColor.CGColor, (__bridge id)self.middleColor.CGColor, (__bridge id)self.endColor.CGColor, (__bridge id)self.endColor.CGColor];
//
//            layer.locations = @[@0.0,
//                                @0.3,
//                                @0.5,
//                                @0.8,
//                                @1.0
//            ];
//            layer.startPoint = CGPointMake(0.0, 1.0);
//            layer.endPoint = CGPointMake(1.0, 0.0);
            
            
            layer.colors = @[(__bridge id)self.startColor.CGColor, (__bridge id)self.endColor.CGColor];
            layer.startPoint = CGPointMake(0.0, 0.0);
            layer.endPoint = CGPointMake(0.0, 1.0);
            
            
            layer.frame = self.bounds;
            
            self.gradientLayer = layer;
            [self.layer insertSublayer:self.gradientLayer atIndex:0];
        } else {
            self.gradientLayer.frame = _previousLayerRect;
        }
    }
}

@end
