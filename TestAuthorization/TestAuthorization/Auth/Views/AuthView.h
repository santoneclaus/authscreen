//
//  AuthView.h
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "GradientView.h"
#import "AuthViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthView : GradientView <AuthViewProtocol>

@end

NS_ASSUME_NONNULL_END
