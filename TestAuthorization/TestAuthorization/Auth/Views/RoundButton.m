//
//  RoundButton.m
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "RoundButton.h"

@interface RoundButton () {
    CGFloat _radius;
}

@end

@implementation RoundButton

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    
    [self configure];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    [self configure];
    
    return self;
}

-  (void)awakeFromNib {
    [super awakeFromNib];
    [self configure];
}

- (instancetype)init {
    self = [super init];
    
    [self configure];
    
    return self;
}

- (void)configure {
    self.layer.masksToBounds = YES;
    if (nil != self.borderColor) {
        self.layer.borderColor = self.borderColor.CGColor;
        self.layer.borderWidth = 1;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat radius = [self getRadius];
    if (radius != _radius) {
        radius = radius;
        self.layer.cornerRadius = radius;
        
        if (0 <= self.value && 10 > self.value) {
            [self setTitle:[NSString stringWithFormat:@"%ld", self.value] forState:UIControlStateNormal];
        }
    }
}

- (CGFloat)getRadius {
    return MIN(self.frame.size.width, self.frame.size.height)/2;
}
@end
