//
//  AuthView.m
//  TestAuthorization
//
//  Created by Alexandr Dubachev on 16.04.2020.
//  Copyright © 2020 Alexandr Dubachev. All rights reserved.
//

#import "AuthView.h"
#import "RoundButton.h"

@interface AuthView ()  {
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinCodLabel;

@property (weak, nonatomic) IBOutlet RoundButton *doneButton;
@property (strong, nonatomic) NSString *pinCod;

@end

@implementation AuthView

@synthesize pinCodLenght;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configure];
}

- (void)configure {
    self.backgroundColor = [UIColor lightGrayColor];
    self.separatorHeightConstraint.constant = 1;//1/MAIN_SCALE;
    [self.doneButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
}

- (void)setPinCod:(NSString *)pinCod {
    _pinCod = pinCod;
    self.pinCodLabel.text = pinCod ? : @"";
}

#pragma mark - AuthViewProtocol
- (void)configureWithTitle:(NSString *)title {
    self.titleLabel.text = title;
    [self clearPinCcod];
}

- (void)clearPinCcod {
    self.pinCod = @"";
}

#pragma mark - Actions
- (IBAction)tapOnButton:(RoundButton *)button {
    [self addSymbol:[NSString stringWithFormat:@"%ld", button.value]];
}

- (IBAction)tapOnRemoveButton:(RoundButton *)button {
    if (self.pinCod.length > 0) {
        self.pinCod = [self.pinCod substringToIndex:self.pinCod.length - 1];
    }
}

#pragma mark -

- (void)addSymbol:(NSString *)symbol {
    if (0 == self.pinCodLenght) {
        self.pinCodLenght = 4;
    }
    if (self.pinCod.length + symbol.length <= self.pinCodLenght) {
        self.pinCod = [NSString stringWithFormat:@"%@%@", self.pinCod, symbol];
        [self checkPincod];
    }
}

- (void)checkPincod {
    if (self.pinCod.length == self.pinCodLenght) {
        [self.doneButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

-  (void)removesubviewFrom:(UIStackView *)stackView {
    NSArray <UIView *>* views = [stackView arrangedSubviews];
    for (UIView *view in views) {
        [stackView removeArrangedSubview:view];
    }
}


@end
